using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiActions : MonoBehaviour
{

    private int counter;

    
    // Start is called before the first frame update
    void Start()
    {
        counter = PlayerPrefs.GetInt("Clicks");
        Debug.Log(counter);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void btnAction(string message )
    {
        counter++;
        PlayerPrefs.SetInt("Clicks", counter);
        Debug.Log(message);
    }

    public void changeScene()
    {
        PlayerPrefs.Save();
        SceneManager.LoadScene("S2");
        SceneManager.LoadScene(1);
    }
}
